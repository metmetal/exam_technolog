
from pydantic import BaseModel, validator

class ModelCodeRayon(BaseModel):
    letter: str
    num_company: int
    num_article: int
    date: int

    @validator("letter")
    def control_letter(cls, value):
        """Controls number of first letters.
        """
        if len(value) != 3:
            raise ValueError('Le code rayon doit contenir trois lettres à son début')
            # Il faudra que vous remplissiez ce code, par defaut il retournera True
        return value.title()

    @validator("num_company")
    def control_num_company(cls, value):
        """Controls the Ssn key.
        """
        if len(str(value)) != 2 :
            raise ValueError('Le numéro de l\'entreprise doit être de deux chiffres' )
        # Il faudra que vous remplissiez ce code, par defaut il retournera True
        return value.title()

    @validator("date")
    def control_date(cls, value):
        """Controls the Ssn key.
        """
        if len(str(value)) != 4:
            raise ValueError('Le numéro de l\'entreprise doit contenir 4 chiffres')
        # Il faudra que vous remplissiez ce code, par defaut il retournera True
        return value.title()



