from fastapi import FastAPI, HTTPException
from model_game import ModelGame
from db import database

app = FastAPI()

@app.get("/games")
def fetch_games():
    db = database()
    results = db.games.find()
    games = []
    for result in results:
        g = {"code_rayon": result["code_rayon"], "console": result["console"],
             "prix": result["prix"], "genre": result["genre"], "description": result["description"]}
        games.append(g)
    return games

@app.post("/games/")
def create_game(game: ModelGame):
    db = database()
    db.games.insert_one(game.dict())
    return game.dict()

@app.get("/games/{code_rayon}")
def fetch_game_by_code_rayon(code_rayon: str):
    db = database()
    result = db.games.find_one({"code_rayon":code_rayon})
    game = {"code_rayon": result["code_rayon"], "console": result["console"],
         "prix": result["prix"], "genre": result["genre"], "description": result["description"]}
    return game

@app.delete("/games/{code_rayon}")
def delete_game_by_code_rayon(code_rayon: str):

    db = database()
    db.games.delete_one({"code_rayon":code_rayon})

@app.put("/games/{code_rayon}")
def update_game_by_code_rayon(code_rayon: str, game: ModelGame):
    db = database()
    result = db.games.update_one({"code_rayon":code_rayon}, {"$set": game.dict()})
    return game.dict()
