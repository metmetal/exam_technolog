"""Module used to fill a mongo database with defined data.
"""

from pymongo import MongoClient

class MongoSeeder:

    def __init__(self):
        host = 'mongodb'
        client = MongoClient(host=f'{host}')
        self.__db = client.registre

    @property
    def db(self):
        return self.__db

    def seed(self):
        """Seeds the database.
        """
        # Clearing collection
        self.db.games.drop()

        # Insert valid and invalid data
        registre = []

        #valid code rayon
        rayon = {"letter": "FRA", "num_company": 12, "num_article": 42, "date": 2021}
        code = rayon.letter + '.' + rayon.num_company + '.' + rayon.num_article +'.'  + rayon.date

        # Valid data
        code_rayon = code
        console = "PlayStation"
        genre = "combat"
        prix = 15
        description = "Très bon jeu"
        game = {"code_rayon": code_rayon, "console": console,
             "prix": prix, "genre": genre, "description": description}
        registre.append(game)


        self.db.games.insert_many(registre)
        cursor = self.db.games.find()
        for game in cursor:
            print(game)

print("Filling DB")
MongoSeeder().seed()

client = MongoClient(host="mongodb")
db = client.registre
print("Done")
